const mongoose = require('mongoose')
// const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // update
  // const room = await Room.findById('6239a4d66e1b227efc17cd53')
  // room.capacity = 20
  // room.save()
  // console.log(room)

  const room = await Room.findOne({ capacity: { $lt: 100 } })
  console.log(room)

  console.log('--------------------------------------------')

  const rooms = await Room.find({ capacity: { $lt: 100 } })
  console.log(rooms)
}

main().then(function () {
  console.log('Finish')
})
