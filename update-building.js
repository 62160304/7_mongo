const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  const newInformaticsBuilding = await Building.findById('6239a4d66e1b227efc17cd52')
  const room = await Room.findById('6239a4d66e1b227efc17cd53')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log('--------------------------------------------')
  console.log(room)
  console.log('--------------------------------------------')
  console.log(informaticsBuilding)

  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(function () {
  console.log('Finish')
})
